package MovieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac021 on 2017/5/15.
  */
object TopRatingsApp extends App{
  val conf = new SparkConf().setAppName("TopRatings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings: RDD[(Int,Double)]=sc.textFile("/Users/mac021/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
    .filter(strs=>strs(1)!="movieId")
    .map(strs=>strs(1).toInt->strs(2).toDouble)

val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
  totalRatingByMovieId.takeSample(false,5).foreach(println)

  val averageRatingByMovieId=ratings.mapValues(v=> v->1)
    .reduceByKey((acc,curr)=>{
      (acc._1+curr._1)->(acc._2+curr._2)
    }).mapValues(kv=>kv._1/kv._2.toDouble)

  val top10=averageRatingByMovieId.sortBy(_._2,false).take(10)
  top10.foreach(println)

  averageRatingByMovieId.takeSample(false,5).foreach(println)

 // ratings.take(5).foreach(println)
  val movies=sc.textFile("/Users/mac021/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
  .filter(strs=>strs(0)!="movieId")
    .map(strs=>strs(0).toInt->strs(1))

  val joined: RDD[(Int,(Double, String))] =averageRatingByMovieId.join (movies)

  joined.takeSample(false, 5).foreach(println)
}
